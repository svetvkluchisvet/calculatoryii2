<?php
return [
    'Key' => 'Ключ',
    'Description' => 'Описание',
    'Activity' => 'Активность',
    'Update Date' => 'Дата обновления',
    'Adding an update application component' => 'Добавление компонента применения обновлений',
    'Update application component' => 'Компонент применения обновлений',
    'Yes' => 'Да',
    'No' => 'Нет',
    'On' => 'Включен',
    'Off' => 'Выключен',
];