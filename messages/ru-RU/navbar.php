<?php
return [
    'Raw material shipping cost calculator' => 'Калькулятор стоимости доставки сырья',
    'Registration' => 'Регистрация',
    'Authorization' => 'Авторизация',
    'History of calculations' => 'История расчетов',
    'Users' => 'Пользователи'
];