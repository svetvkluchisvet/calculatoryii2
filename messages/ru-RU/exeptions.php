<?php
return [
    'Access denied' => 'Доступ запрещен',
    'Enter something' => 'Заполните поле',
    'You can use only russian and english letters' => 'Вы можете использовать только русские и английские буквы.',
    'Password must contain from 6 to 20 characters, it can use numbers, symbols and lettrers, of the latin alphabet. At the same time, the password must contain at least one digit.' => 'Пароль должен содержать от 6 до 20 символов, в нем можно использовать цифры, символы и буквы латинского алфавита. При этом обязательно в пароле должна быть хотя бы одна цифрa.',
    'Password don`t match' => 'Пароли не совпадают!',
    'Incorrect email or password.' => 'Неверный почтовый адресс или пароль'
];