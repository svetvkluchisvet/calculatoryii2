<?php
return [
    'Name & Surname' => 'Имя и Фамилия',
    'Email' => 'Электронный адрес',
    'Password' => 'Пароль',
    'Repeat password' => 'Повторите пароль',
    'Remember me' => 'Запомнить меня',
    'Admin panel' => 'Панель администратора',
    'Update User' => 'Изменениее юзера',
    'Is Admin' => 'Назначить администратором',
    'User ID' => 'ID Пользователя',
    'Created At' => 'Время создания',
    'Table Data' => 'Таблица',
    'Result' => 'Результат',
    'View calculation' => 'Просмотр расчета'
];
