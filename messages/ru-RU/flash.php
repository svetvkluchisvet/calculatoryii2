<?php
return [
    "Hello, {username}, you have logged in to the shipping calculator. Now all your calculations will be saved for later viewing in the calculation log (the text “calculation log” should be displayed as a link to view the history/history log)." => "Здравствуйте, {username}, вы авторизовались в системе расчета стоимости доставки. Теперь все ваши расчеты будут сохранены для последующего просмотра в журнале расчетов (текст “журнале рассчетов” должен отображаться как ссылка на просмотр журнала history/history)."
];