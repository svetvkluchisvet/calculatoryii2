<?php
return [
    'Entry' => 'Войти',
    'Registration' => 'Регистрация',
    'Calculate' => 'Рассчитать',
    'Exit' => 'Выход',
    'Update' => 'Изменить',
];
