<?php

namespace app\models;

use Yii;

class SignupForm extends \yii\base\Model
{
    public $name;
    public $password;
    public $email;
    public $password_repeat;

    public function rules()
    {
        return [
            [['name', 'email', 'password', 'password_repeat'], 'required', 'message' => Yii::t('exeptions', 'Enter something')],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-яё\s]+$/', 'message' => Yii::t('exeptions', 'You can use only russian and english letters')],
            ['email', 'email'],
            [['password'], 'match', 'pattern' => '((?=.*\\d)(?=.*[a-z]).{6,20})', 'message' => Yii::t('exeptions', 'Password must contain from 6 to 20 characters, it can use numbers, symbols and lettrers, of the latin alphabet. At the same time, the password must contain at least one digit.')],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => Yii::t('exeptions', 'Password don`t match')],
        ];
    }

    public function createUser()
    {
        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->password = \Yii::$app->security->generatePasswordHash($this->password);

        return $user;
    }


}