<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EditUserForm extends Model
{
    public $name;
    public $email;
    public $isAdmin;

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string'],
            [['isAdmin'], 'boolean'],
            ['email', 'email'],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-яё\s]+$/', 'message' => Yii::t('exeptions', 'You can use only russian and english letters')],

        ];
    }
}