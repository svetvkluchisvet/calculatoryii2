<?php

namespace app\models;

use yii\db\ActiveRecord;

class Tonnage extends ActiveRecord
{
    public function getTonnage()
    {
        return $this->hasOne(History::className(), ['value' => 'tonnage_id']);
    }
}