<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class HistorySearch extends History
{
    public $monthName;
    public $tonnageValue;
    public $typeName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'result', 'user_id'], 'integer'],
            [['name', 'monthName', 'user_id', 'typeName', 'tonnageValue', 'created_at'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()
            ->joinWith(['tonnage', 'type', 'month']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tonnageValue'] = [
            'asc' => ['tonnage.value' => SORT_ASC],
            'desc' => ['tonnage.value' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['typeName'] = [
            'asc' => ['type.name' => SORT_ASC],
            'desc' => ['type.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['monthName'] = [
            'asc' => ['month.name' => SORT_ASC],
            'desc' => ['month.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'tonnage.value' => $this->tonnageValue,
                'result' => $this->result,
                'created_at' => $this->created_at,
            ]);
        if (Yii::$app->user->can('admin')) {
            $query->andFilterWhere([
                'user_id' => $this->user_id,
            ]);
        } else {
            $query->andWhere([
                'user_id' => Yii::$app->user->id,
            ]);
        }

        $query->andFilterWhere(['like', 'history.name', $this->name])
            ->andFilterWhere(['like', 'month.name', $this->monthName])
            ->andFilterWhere(['like', 'type.name', $this->typeName]);

        return $dataProvider;
    }
}
