<?php

namespace app\models;

use Yii;

class LoginForm extends \yii\base\Model
{
    public $password;
    public $email;
    public $rememberMe = true;

    private $user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, \Yii::t('exeptions', 'Incorrect email or password.'));
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $username = $this->user->name;
            Yii::$app->session->setFlash('success', Yii::t(
                'flash',
                'Hello, {username}, you have logged in to the shipping calculator. Now all your calculations will be saved for later viewing in the calculation log (the text “calculation log” should be displayed as a link to view the history/history log).',
                ['username' => $username]
            ));
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    private function getUser()
    {
        if ($this->user === false) {
            $this->user = User::findByEmail($this->email);
        }
        return $this->user;
    }

    public function attributeLabels()
    {
        return [
            'rememberMe' => Yii::t('form-labels', 'Remember me')
        ];
    }
}
