<?php

namespace app\models\release_control;

class FeatureEnums
{
    /**
     * @var string
     */
    const USE_SOME_NEW_FEATURE = 'use.some_new_feature';

    const USE_VUE_JS_FORM_REALIZATION = "use.vue_js_form_realization";
}