<?php

namespace app\models\release_control;

class ReleaseControlRepository extends \app\models\bases\BaseModelRepository
{
    /**
     * @see BaseModelRepository
     */
    protected static $entityClass = ReleaseControl::class;

    /**
     * @see BaseModelRepository
     */
    protected $tablePrefix = 'rc';

    /**
     * @param string $key
     * @return bool
     */
    public function isActiveExist($key)
    {
        $query = $this->newQuery()
            ->where([
                'key' => $key,
                'active' => ReleaseControlEnum::RELEASE_CONTROL_ACTIVE_VALUE,
            ]);


        return (bool)$query->exists();
    }
}