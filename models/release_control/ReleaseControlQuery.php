<?php

namespace app\models\release_control;

class ReleaseControlQuery extends \yii\db\ActiveQuery
{
    public function __construct($modelClass)
    {
        parent::__construct($modelClass);
    }

    /**
     * @param string $periodFrom
     * @param string $periodTo
     * @return void
     */
    public function defineFilterPeriod($tableName, $periodFrom, $periodTo)
    {
        $this->andFilterWhere(['between', $tableName, $periodFrom, $periodTo]);
    }
}