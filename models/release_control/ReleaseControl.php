<?php

namespace app\models\release_control;


use Yii;
use yii\db\ActiveRecord;

class ReleaseControl extends ActiveRecord
{
    public static function tableName()
    {
        return 'release_control';
    }

    public function rules()
    {
        return [
            [['release_control_id'], 'integer', 'max' => 10],
            [['active', 'locked'], 'integer', 'max' => 3],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'description'], 'string']
        ];
    }

    /**
     * @return ReleaseControlQuery
     */
    public static function find()
    {
        return new ReleaseControlQuery(static::class);
    }

    public function attributeLabels()
    {
        return [
            'release_control_id' => 'ID',
            'active' => Yii::t('release-control', 'Activity'),
            'description' => Yii::t('release-control', 'Description'),
            'updated_at' => Yii::t('release-control', 'Update Date'),

        ];
    }

}