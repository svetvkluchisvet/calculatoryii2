<?php

namespace app\models;

use yii\db\ActiveRecord;

class Month extends ActiveRecord
{
    public function getMonth()
    {
        /** @var TYPE_NAME $this */
        return $this->hasOne(History::className(), ['name' => 'month_id']);
    }
}