<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property string $user_id
 * @property string $name
 * @property string $type_id
 * @property int $tonnage_id
 * @property string $month_id
 * @property int $result
 * @property string $created_at
 * @property string $table_data
 */
class History extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'history';
    }

    public function rules()
    {
        return [
            [['user_id', 'type_id', 'tonnage_id', 'month_id'], 'required'],
            [['result'], 'integer'],
            [['created_at'], 'safe'],
            [['table_data'], 'string'],
            [['user_id', 'tonnage_id'], 'integer', 'max' => 255],
            [['name', 'type_id', 'month_id'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t('form-lables', 'User ID'),
            'name' => Yii::t('form-lables', 'Name & Surname'),
            'type_id' => Yii::t('calculator-lables', 'Material type'),
            'tonnage_id' => Yii::t('calculator-lables', 'Tonnage'),
            'month_id' => Yii::t('calculator-lables', 'Months'),
            'result' => Yii::t('form-lables', 'Result'),
            'created_at' => Yii::t('form-lables', 'Created At'),
        ];
    }

    public function getMonth()
    {
        return $this->hasOne(Month::className(), ['id' => 'month_id']);
    }

    public function getTonnage()
    {
        return $this->hasOne(Tonnage::className(), ['id' => 'tonnage_id']);
    }

    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
}
