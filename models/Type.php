<?php

namespace app\models;

use yii\db\ActiveRecord;

class Type extends ActiveRecord
{
    public function getType()
    {
        return $this->hasOne(History::className(), ['name' => 'type_id']);
    }
}