const path = require('path');

const {VueLoaderPlugin} = require('vue-loader');
const WebpackWatchFilesPlugin = require('webpack-watch-files-plugin').default;


const PATHS = {
    source: path.join(__dirname, 'vue'),
    build: path.join(__dirname, '/web/dist')
};

console.log(__dirname);
console.log(path.resolve(__dirname, 'web/dist'));

module.exports = {
    mode: 'development',
    entry: ["/vue/main.ts"],
    output: {
        path: PATHS.build,
        publicPath: path.resolve(__dirname, '/web/dist'),
        filename: "[name].js",
        clean: true,
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.ts$/,
                loader: "ts-loader",
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                },
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new VueLoaderPlugin(),
        new WebpackWatchFilesPlugin({
            files: ['./vue/**/*'],
        }),
    ],
};
