<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%history}}`.
 */
class m230118_125056_drop_tonnage_name_column_from_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%history}}', 'tonnage_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%history}}', 'tonnage_name', $this->string());
    }
}
