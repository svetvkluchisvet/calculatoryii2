<?php

use yii\db\Migration;

/**
 * Class m230113_123315_table_insert_release_control
 */
class m230113_123315_table_insert_release_control extends Migration
{
    const TABLE_NAME = '{{%release_control}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'key' => 'use.vue_js_form_realization',
            'description' => 'Реализация главной страницы на Vue',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230113_123315_table_insert_release_control cannot be reverted.\n";

        return false;
    }
}
