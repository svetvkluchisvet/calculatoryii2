<?php

use yii\db\Migration;

/**
 * Class m221230_050639_create_table_history
 */
class m221230_050639_create_table_history extends Migration
{
    const TABLE_NAME = 'history';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'type_name' => $this->string()->notNull(),
            'tonnage_name' => $this->string()->notNull(),
            'month_name' => $this->string()->notNull(),
            'result' => $this->integer()->defaultValue(0)->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
