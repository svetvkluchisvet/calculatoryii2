<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%history}}`.
 */
class m230118_124735_add_type_id_column_to_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%history}}', 'type_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%history}}', 'type_id');
    }
}
