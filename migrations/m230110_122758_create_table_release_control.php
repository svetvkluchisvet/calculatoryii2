<?php

use yii\db\Migration;

/**
 * Class m230110_122758_create_table_release_control
 */
class m230110_122758_create_table_release_control extends Migration
{
    const TABLE_NAME = 'release_control';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'release_control_id' => "int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID'",
            'key' => "varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Ключ идентификации'",
            'description' => "varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Описание'",
            'active' => "tinyint(3) unsigned DEFAULT 0 COMMENT 'Активность'",
            'locked' => "tinyint(3) unsigned DEFAULT 0 COMMENT 'Блокировка кнопки'",
            'created_at' => "timestamp NULL DEFAULT NULL COMMENT 'Дата создания'",
            'updated_at' => "timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата обновления'",
            "PRIMARY KEY (release_control_id)",
        ]);

        $this->createIndex(
            'idx-release_control_key',
            'release_control',
            'key'
        );

        $this->createIndex(
            'idx-release_control_active',
            'release_control',
            'active'
        );

        $this->createIndex(
            'idx_release_control_locked',
            'release_control',
            'locked'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
