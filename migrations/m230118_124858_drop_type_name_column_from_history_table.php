<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%history}}`.
 */
class m230118_124858_drop_type_name_column_from_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%history}}', 'type_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%history}}', 'type_name', $this->string());
    }
}
