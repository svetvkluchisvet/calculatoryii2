<?php

use yii\db\Migration;

/**
 * Class m221223_123639_create_table_user
 */
class m221223_123639_create_table_user extends Migration
{
    const TABLE_NAME = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'email' => $this->string(255)->notNull()->unique(),
            'password' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
