<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%history}}`.
 */
class m230118_121447_drop_username_column_from_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%history}}', 'username');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%history}}', 'username', $this->string());
    }
}
