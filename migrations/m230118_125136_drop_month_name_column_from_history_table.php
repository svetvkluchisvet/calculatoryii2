<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%history}}`.
 */
class m230118_125136_drop_month_name_column_from_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%history}}', 'month_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%history}}', 'month_name', $this->string());
    }
}
