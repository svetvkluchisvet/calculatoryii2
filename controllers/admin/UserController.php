<?php

namespace app\controllers\admin;

use app\models\EditUserForm;
use app\models\User;
use app\models\UserSearch;
use Yii;

class UserController extends BaseController
{
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $auth = Yii::$app->authManager;
        $user = User::findOne(['id' => $id]);
        $form = new EditUserForm();
        $form->load($user->toArray(), '');
        $role = $auth->getRole('admin');

        if (Yii::$app->authManager->checkAccess($id, 'admin')) {
            $form->load(['isAdmin' => true], '');
        }

        $form->load($user->toArray(), '');

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $user->email = $form->email;
            $user->name = $form->name;
            $user->save();

            if ($form->isAdmin === '1') {
                $auth->assign($role, $id);
            } else {
                $auth->revoke($role, $id);
            }

            return $this->redirect(['/admin/user']);
        }

        return $this->render('update', ['model' => $form]);
    }

    public function actionDelete($id)
    {
        UserSearch::findOne(['id' => $id])->delete();
        return $this->redirect(['/admin/user']);
    }
}