<?php

namespace app\controllers\admin;

use yii\filters\AccessControl;

abstract class BaseController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ]
        ];
    }

}