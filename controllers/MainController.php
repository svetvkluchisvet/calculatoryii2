<?php

namespace app\controllers;

use app\models\FormDataProps;
use app\models\History;
use app\models\repositories\DataRepository;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

class MainController extends Controller
{
    public function actionIndex()
    {
        $model = new FormDataProps();

        $repository = new DataRepository();
        $months = $repository->findMonths();
        $tonnage = $repository->findTonnages();
        $type = $repository->findTypes();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        return $this->render('index', [
            'type' => $type,
            'tonnage' => $tonnage,
            'months' => $months,
            'model' => $model,
        ]);
    }

    function actionFill()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('/');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new FormDataProps();

        if (!$model->load(Yii::$app->request->post()) || !$model->validate()) {
            return $this->redirect(('/'));
        }

        $repository = new DataRepository();
        $months = $repository->findMonthById($model->months);
        $tonnage = $repository->findTonnageById($model->tonnage);
        $type = $repository->findTypeById($model->type);

        $prices = $repository->findPriceAll();
        $priceData = $repository->findPriceOneByParams($months['id'], $tonnage['id'], $type['id']);

        $priceTable = [];

        foreach ($prices as $item) {
            $priceTable[$item['type_id']][$item['tonnage_id']][$item['month_id']] = $item['value'];
        }

        if (Yii::$app->user->isGuest === false) {
            $history = new History();
            $history->user_id = Yii::$app->user->id;
            $history->name = Yii::$app->user->identity->name;
            $history->month_id = $months['id'];
            $history->tonnage_id = $tonnage['id'];
            $history->type_id = $type['id'];
            $history->result = $priceData['value'];
            $history->table_data = Json::encode([
                'months' => $repository->findMonths(),
                'tonnage' => $repository->findTonnages(),
                'types' => $repository->findTypes(),

                'selectType' => $type,
                'selectMonths' => $months,
                'selectTonnage' => $tonnage,

                'priceTable' => $priceTable,
                'typeKey' => $type['id'],
                'priceValue' => $priceData['value'],
            ]);
            $history->save();
        }

        return $this->renderAjax('form', [
            'months' => $repository->findMonths(),
            'tonnage' => $repository->findTonnages(),
            'types' => $repository->findTypes(),

            'selectType' => $type,
            'selectMonths' => $months,
            'selectTonnage' => $tonnage,

            'priceTable' => $priceTable,
            'typeKey' => $type['id'],

            'priceValue' => $priceData['value'],
        ]);
    }
}
