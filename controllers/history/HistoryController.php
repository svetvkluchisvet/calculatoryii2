<?php

namespace app\controllers\history;

use app\models\History;
use app\models\HistorySearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * @var $dataProvider
 */
class HistoryController extends BaseController
{
    public function actionIndex()
    {
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('history', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->redirect(['/history/history']);
    }

    protected function findModel($id)
    {
        $model = History::findOne($id);


        if ($model === null) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->user->id !== $model->user_id && \Yii::$app->user->can('admin') === false) {
            throw new UnauthorizedHttpException();
        }

        return $model;
    }
}