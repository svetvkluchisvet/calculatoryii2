<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;


class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'logout' => ['post'],
                        ],
                    ],
                ],
            ]
        ];
    }

    public function actionSignup()
    {
        $model = new SignupForm();

        if (Yii::$app->user->isGuest === false) {
            return $this->goHome();
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user = $model->createUser();

            if ($user->save()) {
                return $this->redirect('/user/login');
            }
        }
        return $this->render('signup', compact('model'));
    }

    public function actionLogin()
    {
        if (Yii::$app->user->isGuest === false) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}


