<?php

namespace app\controllers;

use app\components\release_control\ReleaseControlComponent;
use app\models\bases\RepositoryServiceController;
use app\models\release_control\FeatureEnums;
use app\models\release_control\ReleaseControlSearch;
use app\models\release_control\ReleaseControlService;
use Yii;

/**
 * @var $dataProvider
 */
class ReleaseControlController extends RepositoryServiceController
{
    /**
     * @see RepositoryServiceController
     */
    protected static $serviceClass = ReleaseControlService::class;

    /** @var  $repository */
    public function actionIndex()
    {
        $releaseController = \Yii::$container->get(ReleaseControlComponent::class);

        if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true) {
            return $this->render('App.vue');
        }
        $request = Yii::$app->request;

        $searchModel = new ReleaseControlSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $repository = $this->repository;

        $service = $this->service;

        if ($request->isAjax) {
            return $this->renderAjax('_grid_manage',
                compact(
                    'dataProvider',
                    'searchModel',
                    'repository',
                    'service'
                )
            );
        }
        return $this->render('active-feature', [
            'service' => $service,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'repository' => $repository,
        ]);
    }

    public function actionChange()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $active = $request->get('value');

        $this->defineRepositoryModel($id);

        $this->repository->setAttribute('active', (int)filter_var($active, FILTER_VALIDATE_BOOLEAN));

        $this->repository->save(false);
    }
}