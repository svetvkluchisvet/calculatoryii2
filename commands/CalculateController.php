<?php

namespace app\commands;

use app\models\repositories\DataRepository;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;


class CalculateController extends Controller
{
    public $month;
    public $type;
    public $tonnage;

    private $optionsMap = [];

    public function init(): void
    {
        $this->optionsMap = [
            'month' => 'месяц',
            'type' => 'тип сырья',
            'tonnage' => 'тоннаж',
        ];
    }

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), array_keys($this->optionsMap));
    }

    public function validate(): void
    {
        $repository = new DataRepository();
        $months = $repository->findMonths();
        $tonnage = $repository->findTonnages();
        $type = $repository->findTypes();


        $typeValue = $this->upFirstLetter($this->type);
        $monthsValue = $this->upFirstLetter($this->month);
        $tonnageValue = $this->tonnage;

        $errorLines = [];
        foreach ($this->optionsMap as $key => $value) {
            if ($this->{$key} !== null) {
                continue;
            }

            $errorLines[] = $value;
        }

        $this->validateArrayArgument($monthsValue, $months, $_SERVER['argv'][2]);
        $this->validateArrayArgument($typeValue, $type, $_SERVER['argv'][3]);
        $this->validateArrayArgument($tonnageValue, $tonnage, $_SERVER['argv'][4]);


        if (empty($errorLines) === true) {
            return;
        }


        $this->stdout('выполнение команды завершено с ошибкой' . PHP_EOL, Console::FG_RED);

        foreach ($errorLines as $line) {
            throw new \InvalidArgumentException("необходимо ввести {$line}");
        }
    }

    private function validateArrayArgument($argument, $array, $inputValue)
    {
        if (in_array($argument, $array) === false && empty($argument) == false) {
            $this->stdout('выполнение команды завершено с ошибкой' . PHP_EOL, Console::FG_RED);
            throw new \InvalidArgumentException("не найден прайс для значения " . $inputValue . PHP_EOL . 'проверьте корректность введенных значений' . PHP_EOL);
        }
    }

    public function actionIndex(): int
    {
        $repository = new DataRepository();
        $months = $repository->findMonths();
        $tonnage = $repository->findTonnages();
        $type = $repository->findTypes();

        $prices = $repository->findPriceAll();
        // $priceData = $repository->findPriceOneByParams($months['id'], $tonnage['id'], $type['id']);

        $priceTable = [];

        foreach ($prices as $item) {
            $priceTable[$item['type_id']][$item['tonnage_id']][$item['month_id']] = $item['value'];
        }

        $typeValue = $this->upFirstLetter($this->type);
        $monthsValue = $this->upFirstLetter($this->month);
        $tonnageValue = $this->tonnage;


        $keyMonths = array_search($monthsValue, $months);
        $keyType = array_search($typeValue, $type);
        $keyTonnage = array_search($tonnageValue, $tonnage);

        try {
            $this->validate();
        } catch (\InvalidArgumentException $e) {

            $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::DATAERR;
        }

        if ($this->checkKeys($keyTonnage, $keyType, $keyMonths)) {
            $this->stdout("Введенные параметры: " . PHP_EOL, Console::FG_YELLOW);
            $this->stdout("месяц - " . $monthsValue . PHP_EOL, Console::FG_YELLOW);
            $this->stdout("тип - " . $typeValue . PHP_EOL, Console::FG_YELLOW);
            $this->stdout("тонаж - " . $tonnageValue . PHP_EOL, Console::FG_YELLOW);
            //$priceTable[$selectType['id']][$tonnageKey][$monthKey]
            $priceValue = $priceTable[$keyMonths][$keyType][$keyTonnage];
            $this->stdout("результат: " . $priceValue . PHP_EOL, Console::FG_GREEN);

            $builder = new \AsciiTable\Builder();
            foreach ($tonnage as $keyTo => $tonnageValue) {
                $tableData = ['М/Т' => $tonnage[$keyTo]];
                foreach ($months as $keyM => $monthsValue) {
                    $tableData += [$monthsValue => $priceTable[$keyType][$keyTo][$keyM]];
                }
                $builder->addRow($tableData);
            }

            echo $builder->renderTable();
        }
        return ExitCode::OK;
    }

    public function checkKeys($keyTonnage, $keyType, $keyMonths): bool
    {
        return empty($keyTonnage) === false && empty($keyType) === false && empty($keyMonths) === false;
    }

    public function upFirstLetter($str)
    {
        $firstLetter = mb_substr($str, 0, 1, 'UTF-8');
        $lastLetter = mb_substr($str, 1);
        $firstLetter = mb_strtoupper($firstLetter, 'UTF-8');
        $lastLetter = mb_strtolower($lastLetter, 'UTF-8');
        return $str = $firstLetter . $lastLetter;
    }
}
