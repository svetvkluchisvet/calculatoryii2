<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionAddAdmin()
    {
        $model = User::find()->where(['name' => 'admin'])->one();
        if (null === $model) {
            $model = new User();
            $model->name = 'admin';
            $model->email = 'admin@admin.ru';
            $model->setPassword('admin');
            $model->createAuthKey();
            if ($model->save()) {
                $this->stdout('good' . PHP_EOL, Console::FG_YELLOW);
            }
        }

        $auth = Yii::$app->authManager;
        if ($auth->getRole('admin') === null) {
            $admin = $auth->createRole('admin');
            $auth->add($admin);
            $auth->init();
            $role = $auth->getRole('admin');
            $auth->assign($role, $model->id);
        } else $this->stdout('no you cant' . PHP_EOL, Console::FG_YELLOW);
    }
}