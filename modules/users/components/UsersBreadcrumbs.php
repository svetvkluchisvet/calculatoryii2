<?php

namespace app\modules\users\components;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО" (28.09.15 11:25)
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 */
class UsersBreadcrumbs extends Breadcrumbs
{
    protected function renderItem($link, $template)
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? UsersHtml::encode($link['label']) : $link['label'];
        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }
        if (isset($link['url'])) {
            $options = $link;
            unset($options['template'], $options['label'], $options['url']);
            $link = UsersHtml::a($label, $link['url'], $options);
            $link = empty ($link) ? $label : $link;
        } else {
            $link = $label;
        }
        return strtr($template, ['{link}' => $link]);
    }
}