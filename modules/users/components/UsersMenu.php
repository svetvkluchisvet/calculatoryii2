<?php

namespace app\modules\users\components;

use yii\bootstrap\Nav;

/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО"
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 * @class UsersMenu
 * Меню навигации с проверкой
 * доступа для текущего пользователя
 * в items['url'] важно задавать полный машрут без
 * сокращения
 */
class UsersMenu extends Nav
{
    public function init()
    {
        parent::init();
        $this->applyPermissions($this->items);
    }

    /**
     * @param array $items
     * @return bool
     */
    protected function applyPermissions(&$items)
    {
        $allVisible = false;

        foreach ($items as &$item) {
            if (isset($item['url']) && !in_array($item['url'], ['', '#']) && !isset($item['visible'])) {
                $item['visible'] = UsersHelper::UserCan(UsersHelper::Route2rbac($item['url']));
            }

            if (isset($item['items'])) {
                if (!$this->applyPermissions($item['items']) && !isset($item['visible'])) {
                    $item['visible'] = false;
                }
            }

            if (isset($item['label']) && (!isset($item['visible']) || $item['visible'] === true)) {
                $allVisible = true;
            }
        }
        return $allVisible;
    }
} 