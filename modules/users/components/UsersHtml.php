<?php
/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО"
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 */

namespace app\modules\users\components;

use app\modules\users\UserModule;
use yii\helpers\Html;

class UsersHtml extends Html
{
    /**
     * Hide link if user hasn't access to it
     *
     * @inheritdoc
     */
    public static function a($text, $url = null, $options = [])
    {
        if (in_array($url, [null, '', '#'])) {
            return parent::a($text, $url, $options);
        }

        $params = [];
        if (count($url) > 1) {
            $cnt = 1;
            foreach ($url as $name => $value) {
                if ($cnt > 1) {
                    $params[$name] = $value;
                }
                $cnt++;
            }
        }

        return UsersHelper::UserCan(UsersHelper::Route2rbac($url), $params)
            ? parent::a($text, $url, $options)
            : '';
    }

    /**
     * Возвращает кнопку отмены
     * @param array $route маршрут
     * @param string $name
     * @param bool $translate переводить или нет
     * @return string
     */
    public static function cancelButton($route, $name = 'Cancel', $translate = true)
    {
        $def = UserModule::t('users', 'Cancel');
        if ($translate) {
            $name = UserModule::t('users', $name);
        }
        return static::a($name, $route, ['class' => 'btn btn-danger']);
    }

    /**
     * Возвращает кнопку возврата
     * @param string $name
     * @param bool $translate переводить или нет
     * @return string
     */
    public static function backButton($name = 'Back', $translate = true)
    {
        $def = UserModule::t('users', 'Back');
        if ($translate) {
            $name = UserModule::t('users', $name);
        }
        return static::button($name, [
            'class' => 'btn btn-success',
            'onclick' => 'window.history.back();',
        ]);
    }

    /**
     * Возвращает кнопку возврата
     * @param array $route маршрут
     * @param string $name
     * @param bool $translate переводить или нет
     * @return string
     */
    public static function listButton($route = ['manage'], $name = 'List', $translate = true)
    {
        $def = UserModule::t('users', 'List');
        if ($translate) {
            $name = UserModule::t('users', $name);
        }
        return static::a($name, $route, ['class' => 'btn btn-info']);
    }

    /**
     * Generates a button tag.
     * @param string $content the content enclosed within the button tag. It will NOT be HTML-encoded.
     * Therefore you can pass in HTML code such as an image tag. If this is is coming from end users,
     * you should consider [[encode()]] it to prevent XSS attacks.
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
     * If a value is null, the corresponding attribute will not be rendered.
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     * @param string $action идентификатор действия для проверки прав доступа
     * @return string the generated button tag
     */
    public static function button($content = 'Button', $options = [], $action = null)
    {
        if (!is_null($action)) {
            if (!\Yii::$app->authManager->checkAccess(\Yii::$app->user->id, $action)) {
                return '';
            }
        }
        return parent::button($content, $options);

    }
}