<?php
/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО"
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 * Дата: 27.08.15
 * Время: 14:44
 */

namespace app\modules\users\components;

use app\modules\users\models\User;
use app\modules\users\UserModule;
use yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class UsersController extends Controller
{

    public $defaultAction = 'manage';

    /**
     * Перед вызовом действия
     * @param yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {

        //определяем идентификатор запрашиваемого действия
        $id = UsersHelper::Route2rbac(Yii::$app->controller->route);

        if (!UsersHelper::UserCan($id)) {
            if (Yii::$app->user->isGuest) { //если гость
                // отправляем на страницу аутентификации
                Yii::$app->user->loginRequired();
                Yii::$app->end();
            } else { // если авторизированный пользователь
                $user = Yii::$app->user->identity;
                // регистрируем в журнале
                Yii::info("Пользователю #$user->id - $user->shortname " .
                    "было отказано в доступе к $id", 'security');
                // и выкидиываем 403 ошибку
                throw new ForbiddenHttpException(UserModule::t('users',
                    'Access Denied, contact the administrator'));
            }
        }
        return parent::beforeAction($action);
    }

}