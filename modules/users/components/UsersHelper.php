<?php
/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО" (28.08.15 8:39)
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 */

namespace app\modules\users\components;

use app\modules\users\UserModule;
use Yii;

class UsersHelper
{

    /**
     * Преобразует маршрут для создания url
     * в идентификатор разрешения RBAC
     * @param $route
     * @return string
     */
    public static function Route2rbac($route)
    {
        if (is_array($route) && isset($route[0])) {
            $route = $route[0];
        }
        $parts = explode('/', $route);
        if (count($parts) > 0) {
            $route = '';
            $end_parts_index = count($parts) - 1;
            for ($i = 0; $i <= $end_parts_index; $i++) {
                if (!empty($parts[$i])) {
                    if ($i < $end_parts_index) {
                        $route .= $parts[$i] . '/';
                    } else {
                        $route .= $parts[$i];
                    }
                }
            }
        }
        return $route;
    }

    /**
     * Проверяет доступ пользователя
     * @param string $permission_name имя разрешения
     * @param array $param параметры
     * @return bool
     */
    public static function UserCan($permission_name, $param = [])
    {
        $auth = Yii::$app->authManager;
        $perm = $auth->getPermission($permission_name);
        if (is_null($perm)) { // если не существует разрешения в хранилище
            Yii::warning('Для маршрута ' . $permission_name .
                ' не определено разрешение в хранилище RBAC', 'users');
            return UserModule::AllowNotExistsPermissions();
        } else {
            if (Yii::$app->user->isGuest) { // если гость
                $role = $auth->getRole(UserModule::ROLE_GUEST);
                if ($auth->hasChild($role, $perm)) {
                    return true;
                } else return false;
            } else { //аутентифицированный пользователь
                if (Yii::$app->user->can($permission_name, $param)) {
                    return true;
                } else return false;
            }
        }
    }
}