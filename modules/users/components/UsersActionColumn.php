<?php
/**
 * @link http://www.efko.ru/
 * @copyright Copyright (c) ЗАО "Управляющая компания ЭФКО" (02.09.15 8:41)
 * @author веб - программист УИТ, Кулинич Александр a.kulinich@uk.efko.ru
 * @class UsersActionColumn расширяет ActionColumn проверкой доступа
 */

namespace app\modules\users\components;

use Closure;
use Yii;
use yii\grid\ActionColumn;

class UsersActionColumn extends ActionColumn
{
    /**
     * @inheritdoc
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return UsersHtml::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return UsersHtml::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return UsersHtml::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
    }

    /**
     * @inheritdoc
     */
    public function createUrl($action, $model, $key, $index)
    {
        if ($this->urlCreator instanceof Closure) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index);
        } else {
            $route_parts = explode('/', Yii::$app->controller->route);
            $url = '/';
            if (count($route_parts) == 3) { // в модуле
                $url .= $route_parts[0] . '/' . $route_parts[1] . '/' . $action;
            } else {
                $url .= $route_parts[0] . '/' . $action;
            }
            return [
                $url,
                'id' => $model->primaryKey,
            ];
        }
    }

}
