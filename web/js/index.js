$('#calculate-form').on('beforeSubmit', function () {
    var data = $(this).serialize();
    $.ajax({
        url: '/main/fill',
        type: 'POST',
        data: data,
        success: function (res) {
            $('#result-box').html(res);
            $('#calculate-form').trigger('reset');
        },
        error: function () {
            alert('ERROR');
        }
    });
    return false;
});