<?php

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/**
 * @var $content
 */
AppAsset::register($this);
?>


<?php $this->beginPage() ?>

<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php
NavBar::begin([
    'brandLabel' => Yii::t('navbar', 'Raw material shipping cost calculator'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse',
    ],
]);
$items[] = ['label' => Yii::t('navbar', 'History of calculations'), 'url' => '/history/history'];
$items[] = ['label' => Yii::t('navbar', 'Users'), 'url' => '/admin/user'];
$items[] = ['label' => Yii::t('release-control', 'Update application component'), 'url' => '/release-control'];
$items[] = '<li class="divider"></li>';
$items[] = Html::beginForm(['/user/logout'], 'post') . Html::submitButton(Yii::t('button', 'Exit'), ['class' => 'btn center-block']) . Html::endForm();


echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        [
            'label' => Yii::t('navbar', 'Registration'),
            'url' => ['/user/signup']
        ],
        Yii::$app->user->isGuest ? ([
            'label' => Yii::t('navbar', 'Authorization'),
            'url' => ['/user/login']
        ]) : ([
            'label' => Yii::$app->user->identity->name,
            'items' => $items
        ]),
    ],
]);

NavBar::end();
?>
<?php $this->beginBody() ?>
<?= Breadcrumbs::widget([
]);
echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>

<?= Alert::widget() ?>
<?= $content ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

