<?php

use yii\base\View;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);
?>

<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('Login.vue') ?>

<?php else: ?>


    <?php
    /**
     * @var $this View
     * @var $model
     */
    $this->params['breadcrumbs'][] = Yii::t('navbar', 'Authorization');
    if (Yii::$app->user->isGuest === false) {
        $this->params['breadcrumbs'][] = Yii::$app->user->identity->name;
    }
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
    ]); ?>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'email')->textInput()->textInput()->label(Yii::t('form-labels', 'Email')) ?>
    <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('form-labels', 'Password')) ?>
    <?= $form->field($model, 'rememberMe')->checkbox([
        'template' => "<div class=\"offset-lg-1 col-lg-3 custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
    ]) ?>
    <div class="form-group container mt-10">
        <div>
            <?= Html::submitButton(Yii::t('button', 'Entry'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php $form = ActiveForm::end() ?>
<?php endif; ?>
