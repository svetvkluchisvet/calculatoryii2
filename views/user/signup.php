<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);

?>
<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('Signup.vue') ?>

<?php else: ?>

    <?php
    /**
     * @var  $model
     */
    $this->params['breadcrumbs'][] = Yii::t('navbar', 'Registration');

    ?>

    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'name')->textInput()->label(Yii::t('form-labels', 'Name & Surname')) ?>
    <?= $form->field($model, 'email')->textInput()->label(Yii::t('form-labels', 'Email')) ?>
    <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('form-labels', 'Password')) ?>
    <?= $form->field($model, 'password_repeat')->passwordInput()->label(Yii::t('form-labels', 'Repeat password')) ?>

    <div class="form-group container mt-10">
        <div>
            <?= Html::submitButton(Yii::t('button', 'Registration'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php $form = ActiveForm::end() ?>

<?php endif; ?>