<?php

use app\components\release_control\ReleaseControlComponent;
use Tlr\Tables\Elements\Table;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;

$releaseController = \Yii::$container->get(ReleaseControlComponent::class);

?>
<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true) : ?>

    <?php return $this->render('Main.vue'); ?>

<?php else: ?>
    <?php
    /**
     * @var $selectMonths
     * @var $selectTonnage
     * @var $selectType
     * @var $priceValue
     * @var $types
     * @var $typeKey
     * @var $months
     * @var $tonnage
     * @var $priceTable
     */
    $this->params['breadcrumbs'][] = Yii::t('navbar', 'Raw material shipping cost calculator');

    ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@500;900&display=swap" rel="stylesheet">

    <div class="container mt-10">
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <h4 class="text-muted">Месяц</h4>
                    <h2><?= $selectMonths['name'] ?></h2>
                </div>
            </div>
            <div class="col">
                <div class="text-center">
                    <h4 class="text-muted">Тоннаж</h4>
                    <h2><?= $selectTonnage['value'] ?></h2>
                </div>
            </div>
            <div class="col">
                <div class="text-center">
                    <h4 class="text-muted">Тип сырья</h4>
                    <h2><?= $selectType['name'] ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5 text-center">
        <h1 class='text-success' style="font-family: 'Kanit', sans-serif;"><?= $priceValue ?></h1>
    </div>

    <div class="container mt-5 text-center">

        <?php
        $table = new Table;

        $table->class('ui celled table');
        $row = $table->header()->row();
        $row->cell($types[$typeKey]);

        foreach ($months as $key => $month) {
            $row->cell(substr($month, 0, 6));
        }

        foreach ($tonnage as $tonnageKey => $tonnage) {
            $row = $table->body()->row();
            $row->cell($tonnage);
            foreach ($months as $monthKey => $month) {
                $row->cell($priceTable[$selectType['id']][$tonnageKey][$monthKey]);
            }
        }
        echo $table->render();
        ?>
    </div>
<?php endif; ?>