<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);

?>
<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('Main.vue') ?>

<?php else: ?>

    <?php
    /**
     * @var $model
     * @var $type
     * @var $months
     * @var $tonnage
     * @var $priceTable
     */
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'calculate-form',
        'enableAjaxValidation' => true,
    ]); ?>

    <div class='container'>
        <div class="row">
            <form action="index.php" method="POST">
                <div class="mb-5">
                    <?= $form->field($model, 'months', ['enableAjaxValidation' => true])->dropDownList($months, ['prompt' => 'Выберите месяц'])->label(Yii::t('calculator-labels', 'Months')); ?>
                </div>

                <div class="mb-3">
                    <?= $form->field($model, 'tonnage', ['enableAjaxValidation' => true])->dropDownList($tonnage, ['prompt' => 'Выберите тоннаж'])->label(Yii::t('calculator-labels', 'Tonnage')); ?>
                </div>

                <div class="mb-3">
                    <?= $form->field($model, 'type', ['enableAjaxValidation' => true])->dropDownList($type, ['prompt' => 'Выберите тип'])->label(Yii::t('calculator-labels', 'Material type')); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('button', 'Рассчитать'), ['class' => 'btn btn-primary', 'id' => 'you_button_id']) ?>
                </div>
            </form>
        </div>
    </div>
    <?php ActiveForm::end(); ?>


    <div id="result-box"></div>

<?php endif; ?>




