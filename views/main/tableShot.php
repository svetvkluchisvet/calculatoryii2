<?php

use Tlr\Tables\Elements\Table;

?>
<div class="row mt-5">
    <?php
    $table = new Table;

    $table->class('table table-bordered');
    $row = $table->header()->row();
    $row->cell($types[$typeKey]);

    foreach ($months as $key => $month) {
        $row->cell(substr($month, 0, 6));
    }

    foreach ($tonnage as $tonnageKey => $tonnage) {
        $row = $table->body()->row();
        $row->cell($tonnage);
        foreach ($months as $monthKey => $month) {
            $row->cell($priceTable[$selectType['id']][$tonnageKey][$monthKey]);
        }
    }
    echo $table->render();
    ?>
</div>