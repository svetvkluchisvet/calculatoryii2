<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);

?>
<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('AdminUser.vue') ?>

<?php else: ?>

    <?php
/** @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \app\models\UserSearch
 */
$this->params['breadcrumbs'][] = Yii::t('form-lables', 'Admin panel');
?>
<?=
GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'name',
        'email',
        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}'

        ],
    ]

]);
?>
<?php endif; ?>

