<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\User
 */
$this->params['breadcrumbs'][] = Yii::t('form-lables', 'Update User');
?>
<?= $this->render('_form', ['model' => $model]) ?>


