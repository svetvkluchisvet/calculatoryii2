<?php
/**
 * @var $this \yii\web\View
 * @var $model \app\models\User
 */

use yii\helpers\Html;


?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>
<?= $form->field($model, 'name')->textInput()->label(Yii::t('form-lables', 'Name & Surname')) ?>
<?= $form->field($model, 'email')->textInput()->label(Yii::t('form-lables', 'Email')) ?>
<?= $form->field($model, 'isAdmin')->checkbox([
    'template' => "<div class=\"offset-lg-1 col-lg-3 custom-control custom-checkbox\">{input} {Yii::t('form-lables', 'Is Admin')}</div>\n<div class=\"col-lg-8\">{error}</div>",
]) ?>
    <div class="form-group container mt-10">
        <div>
            <?= Html::submitButton(Yii::t('button', 'Update'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
        </div>
    </div>
<?php \yii\widgets\ActiveForm::end() ?>