<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);
?>

<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('History.vue') ?>

<?php else: ?>


    <?php
    /**@var $this yii\web\View */
    /** @var $searchModel app\models\HistorySearch */
    /** @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = Yii::t('navbar', 'History of calculations');
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="history-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                ],
                [
                    'attribute' => 'id',
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'attribute' => 'name',
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Months'),
                    'attribute' => 'monthName',
                    'value' => 'month.name',
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Tonnage'),
                    'attribute' => 'tonnageValue',
                    'value' => 'tonnage.value',
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Material type'),
                    'attribute' => 'typeName',
                    'value' => 'type.name',
                ],
                'result',
                'created_at',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {delete}',
                    'visibleButtons' => [
                        'delete' => \Yii::$app->user->can('admin'),
                    ],
                ],
            ],
        ]);
        ?>
    </div>
<?php endif; ?>