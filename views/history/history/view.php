<?php

use yii\widgets\DetailView;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);
?>

<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('View.vue') ?>


<?php else: ?>

    <?php
    /* @var $this yii\web\View */
    /* @var $model app\models\History */

    $this->params['breadcrumbs'][] = Yii::t('form-lables', 'View calculation');

    \yii\web\YiiAsset::register($this);
    ?>
    <div class="history-view">


        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'id',
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'attribute' => 'name',
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Months'),
                    'value' => $model->month->name,
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Tonnage'),
                    'value' => $model->tonnage->value,
                ],
                [
                    'label' => Yii::t('calculator-lables', 'Material type'),
                    'value' => $model->type->name,
                ],
                'result',
                'created_at',
            ],
        ]) ?>

        <?= $this->render('/main/tableShot', \yii\helpers\Json::decode($model->table_data)); ?>


    </div>
<?php endif; ?>