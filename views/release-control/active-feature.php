<?php

use app\assets\ReleaseControlFilterAsset;
use app\controllers\ReleaseControlController;
use app\models\release_control\FeatureEnums;
use app\components\release_control\ReleaseControlComponent;


$releaseController = \Yii::$container->get(ReleaseControlComponent::class);
?>

<?php if ($releaseController->isEnabled(FeatureEnums::USE_VUE_JS_FORM_REALIZATION) === true): ?>

    <?php return $this->render('App.vue') ?>

<?php else: ?>

    <?php

    /**
     * @var $this \yii\web\View
     * @var $service \app\models\release_control\ReleaseControlService
     * @var $searchModel \app\models\release_control\ReleaseControlSearch
     * @var $dataProvider \yii\data\ActiveDataProvider
     * @var $repository \app\models\release_control\ReleaseControlRepository
     */

    ReleaseControlFilterAsset::register($this);

    $this->title = 'Применение обновлений';
    $this->params['breadcrumbs'][] = ['label' => 'Настройки'];
    $this->params['breadcrumbs'][] = ['label' => 'Применение обновлений'];
    ?>

    <div class="dispatchers-motivation-index">

        <div class="text-center h3">Применение обновлений</div>

        <?= $this->render('_search', [
            'model' => $searchModel,
            'service' => $service,
        ]); ?>

        <?= $this->render('_grid_manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'service' => $service,
            'repository' => $repository,
        ]); ?>

    </div>
<?php endif; ?>